package dmitriy.shchur.HotelApp.views;

import java.lang.reflect.Field;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;

import dmitriy.shchur.HotelApp.entities.Category;
import dmitriy.shchur.HotelApp.entities.Hotel;
import dmitriy.shchur.HotelApp.forms.HotelForm;
import dmitriy.shchur.HotelApp.services.CategoriesService;
import dmitriy.shchur.HotelApp.services.HotelService;

@SuppressWarnings("serial")
@SpringView(name = "")
public class HotelListView extends VerticalLayout implements View {
	
	private CategoriesService categoryService = CategoriesService.getInstance();
	private HotelService service = HotelService.getInstance();
    private VerticalLayout hotels = new VerticalLayout();
	private TextField filterByName = new TextField();
	private TextField filterByAdress = new TextField();
	private Grid<Hotel> grid = new Grid<>(Hotel.class);
	private HotelForm form = new HotelForm(this);
	MultiSelectionModel<Hotel> selectionModel = (MultiSelectionModel<Hotel>) grid.setSelectionMode(SelectionMode.MULTI);
	
	PopupView popup;
    TextField textField = new TextField("Enter new value");
    DateField operatesFrom = new DateField("Operates From");
    NativeSelect<Category> category = new NativeSelect<Category>("Category");
    TextArea description = new TextArea("Description");
	
	public HotelListView(){
		setSizeFull();   	
		
        // Start of Tool bar
        filterByName.setPlaceholder("filter by name...");
        filterByName.addValueChangeListener(e -> updateList());
        filterByName.setValueChangeMode(ValueChangeMode.LAZY);
        filterByName.setDescription("Enter here your query");
        
        filterByAdress.setPlaceholder("filter by adress...");
        filterByAdress.addValueChangeListener(e -> updateList());
        filterByAdress.setValueChangeMode(ValueChangeMode.LAZY);
        filterByName.setDescription("Enter here your query");
        
        	// This button clears both filer fields
        Button clearFilter = new Button(VaadinIcons.CLOSE);
        clearFilter.setDescription("Clear the current filters");
        clearFilter.addClickListener(e -> {filterByName.clear(); filterByAdress.clear();});
        
        	//This is layout for the filter block
        CssLayout filtering = new CssLayout();
        filtering.addComponents(filterByName, filterByAdress, clearFilter);
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        
        
        Button addHotelBtn = new Button("Add new hotel");
        addHotelBtn.addClickListener(e -> {
        	//grid.asSingleSelect().clear();
        	grid.asMultiSelect().clear();
        	form.setHotel(new Hotel());
        });
        
        Button editHotelBtn = new Button("Edit");
        editHotelBtn.addClickListener(e -> {
        	form.setHotel((Hotel)grid.asMultiSelect().getSelectedItems().iterator().next());
        });
        editHotelBtn.setEnabled(false);
        
        Button deleteHotelBtn = new Button("Delete");
        deleteHotelBtn.addClickListener(e ->{
        	for(Hotel hotel:grid.asMultiSelect().getSelectedItems()){
        		service.delete(hotel);
        	}
        	updateList();
        });
        

        
        // Content for the PopupView
        VerticalLayout popupContent = makePopup();
                
        // The PopupView itself
        popup = new PopupView(null, popupContent);
        popup.setHideOnMouseOut(false);
        
        Button bulkUpdate = new Button("BulkUpdate");
        bulkUpdate.addClickListener(click ->
        popup.setPopupVisible(true));
        
        HorizontalLayout toolbar = new HorizontalLayout(filtering, addHotelBtn, editHotelBtn, deleteHotelBtn, bulkUpdate, popup);
        //End of Tool bar
        
        //Start of Hotel Headline
        CssLayout hotelsHeadline = new CssLayout() {
            @Override
            protected String getCss(final Component c) {
                return "font-size: " + 32 + "px";
            }
        };
        Label label = new Label("Hotel List");
        hotelsHeadline.addComponent(label);
        //End of Hotel Headline
        
        //Start of grid
        grid.setColumns("id","name","address","rating","operatesFrom","category");
        grid.addColumn(hotel -> "<a href='" + hotel.getUrl() + "' target='_top'>Link</a>", new HtmlRenderer()).setCaption("Url");
        grid.addColumn("description");

        grid.asMultiSelect().addValueChangeListener(event -> {
        	boolean oneItem = selectionModel.getSelectedItems().size()==1;
        	editHotelBtn.setEnabled(oneItem);
        });
        
        grid.setSizeFull();
        
        
 
        //End of grid
        
        form.setVisible(false);
        
        HorizontalLayout main = new HorizontalLayout(grid, form);
        main.setSizeFull();        
        main.setExpandRatio(grid, 1);
               
        hotels.addComponent(hotelsHeadline);
        hotels.addComponents(toolbar, main);
        
        addComponents(hotels);
        
        updateList();
        
        
		
	}
	
    public void updateList() {
		List<Hotel> hotels = service.findAll(filterByName.getValue(),filterByAdress.getValue());
        grid.setItems(hotels);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		updateList();

	}
	
	private VerticalLayout makePopup(){
        VerticalLayout popupContent = new VerticalLayout();
        
		// Content for the PopupView
        
        List<String> columns = new ArrayList<String>();
        Button applyNewValue = new Button("Apply");
        ComboBox<String> columnsSelect = new ComboBox<String>();
        
        applyNewValue.addClickListener(e -> {
        	Field field = null;
        	Optional<String> item = columnsSelect.getSelectedItem();
        	String itemName = item.get();
        	try{
        	switch (itemName) {
        	case "Name": field = Hotel.class.getDeclaredField("name"); break;
        	case "Address": field = Hotel.class.getDeclaredField("address"); break;
        	case "Rating": field = Hotel.class.getDeclaredField("rating"); break;
        	case "OperatesFrom": field = Hotel.class.getDeclaredField("operatesFrom"); break;
        	case "Category": field = Hotel.class.getDeclaredField("category"); break;
        	case "Url": field = Hotel.class.getDeclaredField("url"); break;
        	case "Description": field = Hotel.class.getDeclaredField("description"); break;
        	default: break;
        	}
        	} catch(Exception ex){
        		ex.printStackTrace();
        	}
        	field.setAccessible(true);
        	Set<Hotel> hotels= selectionModel.getSelectedItems();
        	Object ob = getNewValue(itemName);
        	if (ob!=null){
        	for(Hotel hotel:hotels){
        	try{
        		field.set(hotel, ob);
        	} catch(Exception ex){
        		ex.printStackTrace();
        	}
        	service.save(hotel);
        	}
        	}
        	columnsSelect.clear();
        	operatesFrom.clear();
        	category.clear();
        	textField.clear();
        	description.clear();
        	popup.setPopupVisible(false);
        	updateList();
        });
        
        category.setItems(categoryService.getAll());
        columns.add("Name");
        columns.add("Address");
        columns.add("Rating");
        columns.add("OperatesFrom");
        columns.add("Category");
        columns.add("Url");
        columns.add("Description");
        applyNewValue.setEnabled(false);
    	operatesFrom.setVisible(false);
    	category.setVisible(false);
    	textField.setVisible(false);
    	description.setVisible(false);    	    	
    	
    	columnsSelect.setItems(columns);
        columnsSelect.setPlaceholder("Select a column");
        
        columnsSelect.addValueChangeListener(event -> {
        	String value = event.getValue();
        	boolean textValue,catValue,dateValue,descriptionValue = false;
        	textValue = value=="Url"||value=="Rating"||value=="Address"||value=="Name";
        	catValue = value=="Category";
        	dateValue = value=="OperatesFrom";
        	descriptionValue = value=="Description";
        	     	
        	applyNewValue.setEnabled(value!=null);
        	operatesFrom.setVisible(dateValue);
        	category.setVisible(catValue);
        	description.setVisible(descriptionValue);
        	textField.setVisible(textValue);
        });
        
        popupContent.addComponents(columnsSelect, textField, operatesFrom, category, description);
        popupContent.addComponent(applyNewValue);
        
        return popupContent;
	}

	private Object getNewValue(String itemName) {
		switch(itemName){
		case "OperatesFrom":{
			LocalDate newDate = operatesFrom.getValue();
			if(newDate == null) return null;
			long days = Duration.between(newDate.atTime(0,0), LocalDate.now().atTime(0, 0)).toDays();
			if (days>=0) return days; else {
				Notification.show("Enter data before today");
				return null;
			}
		}
		case "Category":{
			if(category.getSelectedItem().isPresent()) return category.getSelectedItem().get();	else {
				Notification.show("Choose category from list");
				return null;
			}		
		}
		case "Description":{
			return description.getValue();
		}
		case "Rating":{
			String text = textField.getValue();
			if(text.matches("[1-5]")) return Integer.parseInt(text); else {
				Notification.show("Rating should be from 1 to 5");
				return null;
			}					
		}		
		default: return textField.getValue(); 
		}		
		
	}
	
	

}
