package dmitriy.shchur.HotelApp.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.VerticalLayout;

import dmitriy.shchur.HotelApp.forms.CategoriesForm;

@SuppressWarnings("serial")
@SpringView(name = "categories")
public class CategoriesView extends VerticalLayout implements View {
	
	private CategoriesForm categoriesForm = new CategoriesForm();
	
	HotelListView hotelListView;
	
	public CategoriesView(){
        addComponent(categoriesForm);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
