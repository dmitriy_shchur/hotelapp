package dmitriy.shchur.HotelApp;

import javax.servlet.annotation.WebServlet;

import org.springframework.context.annotation.Configuration;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import dmitriy.shchur.HotelApp.views.CategoriesView;
import dmitriy.shchur.HotelApp.views.HotelListView;


/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@SuppressWarnings("serial")
@Theme("mytheme")
@SpringUI
public class MyUI extends UI{
	
    @Configuration
    @EnableVaadin
    public static class MyConfiguration {

    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        final CssLayout topBar = new CssLayout();
        final CssLayout viewLayout = new CssLayout();
        MenuBar barmenu = new MenuBar();
                
        topBar.addComponent(barmenu);
                
        layout.addComponent(topBar);
        layout.addComponent(viewLayout);
        viewLayout.setSizeFull();
        
        getPage().setTitle("Hotel List");
       
        final Navigator navigator = new Navigator(this, viewLayout);
        navigator.addView("", new HotelListView());
        navigator.addView("categories", new CategoriesView());
        
		// Start of Menu bar
        barmenu.addItem("Hotel List",    e -> {
        	navigator.navigateTo("");
        });
        barmenu.addItem("Hotel Categories",  e -> {
        	navigator.navigateTo("categories");
        });
        // End of Menu bar      
        
        setContent(layout); 

    }
    


    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends SpringVaadinServlet {
    }

}
