package dmitriy.shchur.HotelApp.forms;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import dmitriy.shchur.HotelApp.converters.DateConverter;
import dmitriy.shchur.HotelApp.entities.Category;
import dmitriy.shchur.HotelApp.entities.GuarantyFee;
import dmitriy.shchur.HotelApp.entities.Hotel;
import dmitriy.shchur.HotelApp.services.CategoriesService;
import dmitriy.shchur.HotelApp.services.HotelService;
import dmitriy.shchur.HotelApp.views.HotelListView;

@SuppressWarnings("serial")
public class HotelForm extends FormLayout{
	
	private TextField name = new TextField("Name");
	private TextField address = new TextField("Address");
	private TextField rating = new TextField("Rating");
	private DateField operatesFrom = new DateField("Operates From");
	private NativeSelect<Category> category = new NativeSelect<>("Category");
	private TextField url = new TextField("Url");
	private Button save = new Button("Save");
	private Button delete = new Button("Delete");
	private TextArea description = new TextArea("Description");
	private GuarantyFeeField guarantyFee = new GuarantyFeeField();
	
	private HotelService service = HotelService.getInstance();
	private CategoriesService categoryService = CategoriesService.getInstance();
	private Hotel hotel;
	private HotelListView hotelListView;
	private Binder<Hotel> binder = new Binder<>(Hotel.class);
	
	public HotelForm(HotelListView hotelListView){
		this.hotelListView = hotelListView;
		//Descriptions for the hotel form
		description.setDescription("Put here your commentary");
		name.setDescription("Name of the hotel");
		name.setRequiredIndicatorVisible(true);
		address.setDescription("Address of the hotel");
		address.setRequiredIndicatorVisible(true);
		rating.setDescription("Hotel rating by visitors form 1 to 5");
		rating.setRequiredIndicatorVisible(true);
		operatesFrom.setDescription("Opening date");
		operatesFrom.setRequiredIndicatorVisible(true);
		category.setDescription("Category of the hotel");
		category.setRequiredIndicatorVisible(true);
		url.setDescription("Link for more information");
		url.setRequiredIndicatorVisible(true);
		save.setDescription("Press to save the current hotel fields");
		delete.setDescription("Press to delete the current hotel from the system");
		
        guarantyFee.setDescription("Payment method");
        guarantyFee.setCaption("Payment method");
        guarantyFee.addValueChangeListener(v -> {
        	String oldValueStr = getStr(v.getOldValue());
            String valueStr = getStr(v.getValue());   
        	Notification.show("Payment method" + System.lineSeparator() + oldValueStr + " changed on " + valueStr);

        });
				
		setSizeUndefined();
		HorizontalLayout buttons = new HorizontalLayout(save, delete);
		addComponents(name, address, rating, guarantyFee, operatesFrom, category, url, description, buttons);
		
		category.setItems(categoryService.getAll());
		
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(KeyCode.ENTER);
		
		save.addClickListener(e -> this.save());
		delete.addClickListener(e -> this.delete());
				
		bindFields();
	}
	
	private String getStr(GuarantyFee value){
		String valueStr;
		if(value.getGuarantyFee() == null){
			valueStr = "Cash";
		}else{
			valueStr = "Credit Card " + value.getGuarantyFee();
		}
		return valueStr;
	}
	
	private void bindFields() {
		binder.forField(name).asRequired("Enter here the name")
		.bind(Hotel::getName, Hotel::setName);
		binder.forField(address).asRequired("Enter here the address")
		.bind(Hotel::getAddress, Hotel::setAddress);
		binder.forField(rating).withConverter(new StringToIntegerConverter(0, "Only digits!"))
		.withValidator(v ->(v>0),"Rating shoud be a positive number")
		.withValidator(v ->(v<6),"Rating shoud be less than 6")
		.bind(Hotel::getRating, Hotel::setRating);
		binder.forField(operatesFrom).withConverter(new DateConverter())
		.withValidator(v ->(v>0),"The opening date must be before today's date")
		.bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
		binder.forField(url).asRequired("Inter here the url").bind(Hotel::getUrl, Hotel::setUrl);
		binder.forField(category).asRequired("Choose the category").bind(Hotel::getCategory, Hotel::setCategory);
        binder.forField(guarantyFee).asRequired("field can not be empty")
        .withValidator(v -> (v.getGuarantyFee() == null||(v.getGuarantyFee() >= 1 && v.getGuarantyFee() <=100)), "Enter percent of fee")
        .bind(Hotel::getGuarantyFee, Hotel::setGuarantyFee);
		
						
		binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
	}
	
	public void setHotel(Hotel hotel){
		category.setItems(categoryService.getAll());
		this.hotel = hotel;
		binder.readBean(hotel);
		delete.setVisible(hotel.isPersisted());
		setVisible(true);
		name.selectAll();
	}
	
	private void delete(){
		service.delete(hotel);
		hotelListView.updateList();
		setVisible(false);
	}
	
	private void save(){
		if (binder.isValid()){	
		try {
			binder.writeBean(hotel);
		} catch (ValidationException e) {
			e.printStackTrace();
		}
		service.save(hotel);
		hotelListView.updateList();
		setVisible(false);		
		binder.removeBean();
		}
	}

}
