package dmitriy.shchur.HotelApp.forms;

import java.util.Set;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import dmitriy.shchur.HotelApp.entities.Category;
import dmitriy.shchur.HotelApp.services.CategoriesService;

@SuppressWarnings("serial")
public class CategoriesForm extends FormLayout{
	private Button newBtn = new Button("New");
	private Button editBtn = new Button("Edit");
	private Button deleteBtn = new Button("Delete");
	private TextField editField = new TextField();
	private Button saveBtn = new Button(VaadinIcons.CHECK);
	private Button cancelBtn = new Button(VaadinIcons.BAN);
	private ListSelect<Category> categories = new ListSelect<Category>();
	private HorizontalLayout buttons = new HorizontalLayout();
	private CssLayout editing = new CssLayout();
	
	private CategoriesService categoryService = CategoriesService.getInstance();
		
	public CategoriesForm(){

        //Headline of CategoriesForm
        CssLayout categoriesHeadline = new CssLayout() {
            @Override
            protected String getCss(final Component c) {
                return "font-size: " + 32 + "px";
            }
        };
        Label label = new Label("Hotel Categories");
        categoriesHeadline.addComponent(label);
		
        deleteBtn.addListener(e -> this.delete());
        newBtn.addListener(e -> this.newCategory());
        saveBtn.addListener(e -> this.save());
        saveBtn.setDescription("Save category");
        cancelBtn.addListener(e -> this.cancel());
        cancelBtn.setDescription("Cancel");
        editBtn.addListener(e -> this.edit());
        
		buttons.addComponents(newBtn,editBtn,deleteBtn);
		
		categories.setItems(categoryService.getAll());
		categories.setWidth("235px");
		
		categories.addValueChangeListener(event -> {
			Set<Category> selected = event.getValue();
			editBtn.setEnabled(selected.size()==1);
		});
		
		
		editing.addComponents(editField,saveBtn,cancelBtn);
		editing.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		editing.setVisible(false);
		
				
		addComponent(categoriesHeadline);
		addComponent(buttons);
		addComponent(categories);
		addComponent(editing);
	
	}
	
	private void delete(){

		categoryService.delete(categories.getSelectedItems());
		updateList();
	}
	
	private void newCategory(){
		categories.deselectAll();
		categories.setEnabled(false);
		buttons.setEnabled(false);
		editing.setVisible(true);		
	}
	
	private void edit(){
		categories.setEnabled(false);
		buttons.setEnabled(false);
		
		editField.setValue(categories.getSelectedItems().iterator().next().toString());
		
		editing.setVisible(true);		
	}
	
	private void save(){
		if (categories.getSelectedItems().isEmpty()){
			categoryService.save(new Category(editField.getValue()));
		} else {
			categoryService.edit(categories.getSelectedItems().iterator().next(), new Category(editField.getValue()));
		}
			
		
		editing.setVisible(false);	
		categories.setEnabled(true);
		buttons.setEnabled(true);
		
		editField.clear();
		updateList();
	}
	
	private void cancel(){
		editing.setVisible(false);	
		categories.setEnabled(true);
		buttons.setEnabled(true);
		
		editField.clear();
	}
	
	private void updateList(){
		categories.setItems(categoryService.getAll());
	}
}
      