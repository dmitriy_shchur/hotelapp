package dmitriy.shchur.HotelApp.forms;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Label;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import dmitriy.shchur.HotelApp.entities.GuarantyFee;

@SuppressWarnings("serial")
public class GuarantyFeeField extends CustomField<GuarantyFee> {
	
	private GuarantyFee value = new GuarantyFee();
	private Label label = new Label("Payment will be made cash");
	private TextField textField = new TextField();
	private RadioButtonGroup<String> rbg = new RadioButtonGroup<String>();

	@Override
	public GuarantyFee getValue() {

		return value;
	}

	@Override
	protected Component initContent() {
		VerticalLayout field = new VerticalLayout();
		field.setMargin(false);
		label.setSizeFull();
		rbg.setItems("Credit Card", "Cash");
		rbg.setSelectedItem("Cash");
		rbg.setStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		rbg.setSizeFull();
		textField.setDescription("Guaranty fee percent");
		textField.setPlaceholder("Guaranty Deposit");
		textField.setVisible(false);
		textField.setSizeFull();
		
		
		rbg.addSelectionListener(e -> {
			String sel = e.getValue();
			if (sel.equals("Cash")){
				if(textField.getValue().equals("")){ 
					textField.setValue("0");
				}
				textField.clear();
			} else {
				if(value.getGuarantyFee() == null && textField.getValue().equals("")){ 
					textField.setValue("0");
				}
				if(value.getGuarantyFee() == null){
					textField.setValue("");
				}else{
					textField.setValue(value.getGuarantyFee().toString());
				}
			}
				textField.setVisible(sel.equals("Credit Card"));
				label.setVisible(sel.equals("Cash"));
		});
		
	
		textField.addValueChangeListener(e -> {
			getNewValue();
		});
		
		field.addComponents(rbg, label, textField);
		
		return field;
	}
	
	private void getNewValue() {
		String fieldValue = textField.getValue();
		if ((fieldValue.matches("\\d+")) && (Integer.parseInt(fieldValue) <= 100)){
			setNewValue(Integer.parseInt(fieldValue));
		} else if (fieldValue.equals("")){
			if(rbg.getSelectedItem().get().equals("Cash")){
				setNewValue(null);
			} else {
				setNewValue(0);//for ValueChangeListener in HotelForm
			}
		} else {
			textField.clear();
		}
	}
	
	private void setNewValue(Integer newValue){
		GuarantyFee oldValue = new GuarantyFee(value.getGuarantyFee());
		value.setGuarantyFee(newValue);
		fireEvent(new ValueChangeEvent<GuarantyFee>(this, oldValue, false));
	}

	@Override
	protected void doSetValue(GuarantyFee newValue) {
		if(newValue==null){
			value.setGuarantyFee(null);
		} else {
			value.setGuarantyFee(newValue.getGuarantyFee());
		}
		rbg.setSelectedItem("Cash");
		updateGuarantyFee();
	}
	
	private void updateGuarantyFee(){
		if (value.getGuarantyFee() == null){
			rbg.setSelectedItem("Cash");
		} else {
			rbg.setSelectedItem("Credit Card");
		}
	}

}
