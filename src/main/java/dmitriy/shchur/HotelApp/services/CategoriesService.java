package dmitriy.shchur.HotelApp.services;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dmitriy.shchur.HotelApp.entities.AbstractEntity;
import dmitriy.shchur.HotelApp.entities.Category;

public class CategoriesService {
	
	private static CategoriesService instance;
	private static final Logger LOGGER = Logger.getLogger(HotelService.class.getName());
	
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo_hotels");;

    EntityManager entityManager = entityManagerFactory.createEntityManager();
	
	private CategoriesService() {
	}

	public static CategoriesService getInstance() {
		if (instance == null) {
			instance = new CategoriesService();
		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized List<Category> getAll() {
		Query query = entityManager.createNamedQuery("findCategories", Category.class);
		return query.getResultList();
	}
	
	public synchronized String getByIndex(Long id) {
		Query query = entityManager.createNamedQuery("findCategoryByIndex", Category.class);
		query.setParameter("id", "%" + id + "%");
		return ((Category) query.getSingleResult()).toString();
	}
	
	public synchronized Long getIndex(String name) {
		Query query = entityManager.createNamedQuery("findCategoryIndex", Category.class);
		query.setParameter("name", "%" + name + "%");
		return ((AbstractEntity) query.getSingleResult()).getId();
	}
	
	public synchronized void save(Category entry) {
		EntityTransaction entityTransaction = entityManager.getTransaction();
		if (entry == null) {
			LOGGER.log(Level.SEVERE, "Category is null.");
			return;
		}
		try {
			entityTransaction.begin();
			if (entry.getId() == null) {
				entityManager.persist(entry);
			} else {
				entityManager.merge(entry);
			}
			entityTransaction.commit();
		} catch (Exception e) {
			entityTransaction.rollback();
			throw e;
		}
	}
		
	public synchronized void delete(Set<Category> set) {
		for (Category value:set){
			delete(value);		
		}
	}
	
	public synchronized void delete(Category value) {
		EntityTransaction entityTransaction = entityManager.getTransaction();
			try {
				entityTransaction.begin();
				if (entityManager.contains(value)){
					entityManager.remove(value);
				} else {
					entityManager.remove(entityManager.merge(value));
				}
				entityTransaction.commit();
			} catch (Exception e) {
				entityTransaction.rollback();
				throw e;
			}					
	}
	
	public synchronized void edit(Category old, Category newCategory) {
		Category temp = old;
		temp.setName(newCategory.getName());
		save(temp);
	}
	
}
