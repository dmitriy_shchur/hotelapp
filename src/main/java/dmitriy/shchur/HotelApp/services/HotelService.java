package dmitriy.shchur.HotelApp.services;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dmitriy.shchur.HotelApp.entities.Hotel;

public class HotelService {

	private static HotelService instance;
	private static final Logger LOGGER = Logger.getLogger(HotelService.class.getName());
	
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo_hotels");;

    EntityManager entityManager = entityManagerFactory.createEntityManager();
    EntityTransaction entityTransaction = entityManager.getTransaction();

	private HotelService() {
	}

	public static HotelService getInstance() {
		if (instance == null) {
			instance = new HotelService();
		}
		return instance;
	}
		
	//Shows all hotels
	@SuppressWarnings("unchecked")
	public synchronized List<Hotel> findAll() {
		Query query = entityManager.createNamedQuery("findAllHotels", Hotel.class);
		return query.getResultList();
	}
	
	//Finds hotels that match given name and address
	@SuppressWarnings("unchecked")
	public synchronized List<Hotel> findAll(String name, String address) {
		Query query = entityManager.createNamedQuery("findAllFilteredHotels", Hotel.class);
		query.setParameter("name", "%" + name + "%");
		query.setParameter("address", "%" + address + "%");
		return query.getResultList();
	}
	
	
	//Finds hotels that match given name and address with limits
	public synchronized List<Hotel> findAll(String name, String address, int start, int maxresults) {
		ArrayList<Hotel> arrayList = (ArrayList<Hotel>) findAll(name,address);
		int end = start + maxresults;
		if (end > arrayList.size()) {
			end = arrayList.size();
		}
		return arrayList.subList(start, end);
	}

	
	//Delete a hotel in the system
	public synchronized void delete(Hotel value) {
		try {
			entityTransaction.begin();
			if (entityManager.contains(value)){
				entityManager.remove(value);
			} else {
				entityManager.remove(entityManager.merge(value));
			}
			entityTransaction.commit();
		} catch (Exception e) {
			entityTransaction.rollback();
			throw e;
		}
	}
	
	//Save or update a hotel in the system
	public synchronized void save(Hotel entry) {
		if (entry == null) {
			LOGGER.log(Level.SEVERE, "Hotel is null.");
			return;
		}
		try {
			entityTransaction.begin();
			if (entry.getId() == null) {
				entityManager.persist(entry);
			} else {
				entityManager.merge(entry);
			}
			entityTransaction.commit();
		} catch (Exception e) {
			entityTransaction.rollback();
			throw e;
		}
	}

}

