package dmitriy.shchur.HotelApp.converters;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import dmitriy.shchur.HotelApp.entities.Category;
import dmitriy.shchur.HotelApp.services.CategoriesService;

@SuppressWarnings("serial")
public class CategoryConverter implements Converter<Category, Integer> {
	
	private CategoriesService categoryService = CategoriesService.getInstance();

	@Override
	public Result<Integer> convertToModel(Category value, ValueContext context) {
		if (value==null){
			Result.ok(-1);
		}
		return Result.ok(categoryService.getAll().indexOf(value));
	}

	@Override
	public Category convertToPresentation(Integer value, ValueContext context) {
		int size = categoryService.getAll().size();
		if((value==null)||value>=size){
			return new Category("");
		}
		return categoryService.getAll().get(value);
	}

}
