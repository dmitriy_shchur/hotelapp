package dmitriy.shchur.HotelApp.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class GuarantyFee {
	
	@Column(name = "guaranty_fee")
	private Integer guarantyFee;
	
	public Integer getGuarantyFee(){
		return guarantyFee;
	}
	
	public void setGuarantyFee(Integer guarantyFee){
		this.guarantyFee = guarantyFee;
	}
	
	public GuarantyFee(Integer guarantyFee){
		super();
		this.guarantyFee = guarantyFee;
	}
	
	public GuarantyFee(){
		super();
	}
}
