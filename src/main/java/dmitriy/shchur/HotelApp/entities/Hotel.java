package dmitriy.shchur.HotelApp.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "HOTEL")
@NamedQueries({
	@NamedQuery(name = "findAllHotels",query = "SELECT h FROM Hotel h"),
	@NamedQuery(name = "findAllFilteredHotels",query = "SELECT h FROM Hotel h WHERE h.name LIKE :name AND h.address LIKE :address")
	})
public class Hotel extends AbstractEntity implements Serializable, Cloneable {
	
	
	@Column
	private String name = "";
	
	@NotNull
	@Column
	private String address = "";
	
	@NotNull
	@Column
	private Integer rating=0;
	
	@NotNull
	@Column(name="operates_from")
	private Long operatesFrom;

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id")
	private Category category;
	
	@NotNull
	@Column
	private String url="";
	
	@Column
	private String description = "";
	
	@Embedded
	private GuarantyFee guarantyFee;

	public boolean isPersisted() {
		return getId() != null;
	}

	@Override
	public String toString() {
		return name + " " + rating +"stars " + address;
	}

	@Override
	protected Hotel clone() throws CloneNotSupportedException {
		return (Hotel) super.clone();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public Hotel() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Long getOperatesFrom() {
		return operatesFrom;
	}

	public void setOperatesFrom(Long operatesFrom) {
		this.operatesFrom = operatesFrom;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String comment) {
		this.description = comment;
	}
	
	public GuarantyFee getGuarantyFee() {
		return guarantyFee;
	}

	public void setGuarantyFee(GuarantyFee guarantyFee) {
		this.guarantyFee = guarantyFee;
	}

	public Hotel(String name, String address, Integer rating, Long operatesFrom, Category category, String url) {
		super();
		this.name = name;
		this.address = address;
		this.rating = rating;
		this.operatesFrom = operatesFrom;
		this.category = category;
		this.url = url;
	}

}