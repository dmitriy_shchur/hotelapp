package dmitriy.shchur.HotelApp.entities;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "CATEGORY")
@NamedQueries({
	@NamedQuery(name = "findCategories",query = "SELECT c FROM Category c"),
	@NamedQuery(name = "findCategoryByIndex",query = "SELECT c FROM Category c WHERE c.id = :id"),
	@NamedQuery(name = "findCategoryIndex",query = "SELECT c FROM Category c WHERE c.name = :name"),
})
public class Category extends AbstractEntity {
	
	@NotNull
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category(String name) {
		this.name = name;
	}

	public Category() {
		this.name="";
	}
	
	@Override
	public String toString() {
		return name;
	}

}
